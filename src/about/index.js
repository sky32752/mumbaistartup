import React, { Component } from "react";
import Lottie from "react-lottie";
import AboutAnimation from "../img/about.json";
const TweenMax = window.TweenMax;
const ScrollMagic = window.ScrollMagic;

class About extends Component {
  constructor(props) {
    super(props);
    this.controller = new ScrollMagic.Controller();
  }

  componentDidMount() {
    this.scene = new ScrollMagic.Scene({
      triggerElement: "#about",
      offset: 100,
    })
      .setTween(
        TweenMax.staggerFromTo(
          [".about-title", ".about-desc"],
          1,
          {
            y: 40,
            opacity: 0,
          },
          {
            y: 0,
            opacity: 1,
          },
          0.5
        )
      )
      .addTo(this.controller);

    this.scene = new ScrollMagic.Scene({
      triggerElement: "#about",
      offset: 100,
    })
      .setTween(
        TweenMax.from(".about-image", 1, {
          right: -33,
        })
      )
      .addTo(this.controller);
  }

  render() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: AboutAnimation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };
    return (
      <div className="container-fluid p-5">
        <div id="about" className="anchor" />
        <div className="row">
          <div className="col-md-6 p-0 about-text">
            <h1 className="text-left about-title black-text section-title mt-5">
              About Us
            </h1>
            <h5 className="py-3 about-desc answer">
              <em>
                Hey there
              </em>
            </h5>
          </div>
          <div className="col-md-6 p-0 about-image">
            <img
              style={{
                height: 350,
                width: 250,
                marginLeft: 50,
              }}
              src={require("../img/about_us.png")}
              alt="Italian Trulli"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default About;
