import React from "react";
import { Container, Typography, Box } from "@material-ui/core";
import { isMobile } from "react-device-detect";
import Lottie from "react-lottie";
import animationData from "../img/delivery.json";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

class Startup extends React.Component {
  render() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: animationData,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };
    return (
      <Box
        style={{
          display: "flex",
          flexDirection: isMobile ? "column" : "row",
          marginTop: 50,
          backgroundImage: `url(${require("../img/startup_bg.png")})`,
        }}
      >
        <Container
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        >
          {" "}
          <Lottie options={defaultOptions} height={400} width={400} />
        </Container>

        <Container
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            // background: "linear-gradient(to right bottom, #757f9a  , #d7dde8)",
            justifyContent: "center",
            alignContent: "center",
            borderRadius: 20,
          }}
        >
          <Typography variant="h4" style={{ marginBottom: 20 }}>
            <p
              style={{
                display: "inline",

                background: `linear-gradient(45deg, ${"#afedd6"} 30%, ${"#afedb2"} 90%)`,
                padding: 10,
                margin: 2,
                color: "#007054",
                borderRadius: 10,
              }}
            >
              SAFETY
            </p>
            is always our first priority
          </Typography>

          <Typography style={{ color: "#2c3e50" }}>
            The reason to start this services is to let people get the services
            they need delivered at home. So out first priority is the customers
            and thus we always have a very hygiene delivery guy at your gate.
            The delivery guy will always wear gloves and mask . Also ensuring
            he/she doesn't come in contact with the customers in any
            circumstance. These precautions are just to avoid COVID - 19 from
            spreading, also we don't employ anyone with symptomes of COVID - 19
            or any person who might have come in contact of a patient.
          </Typography>
        </Container>
      </Box>
    );
  }
}

export default Startup;
