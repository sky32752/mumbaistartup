import React from "react";
import {
  Box,
  Card,
  CardActionArea,
  CardMedia,
  Typography,
  CardContent,
  Grid,
  Paper,
  Container,
} from "@material-ui/core";

const shops = [
  {
    name: "Aditya Corporation",
    desc: "description here",
    img:
      "https://img.etimg.com/thumb/width-640,height-480,imgsize-789754,resizemode-1,msid-73320353/the-kirana-is-a-technology-shop-too.jpg",
  },
  {
    name: "Bombay Dyeing",
    desc: "description here",
    img:
      "https://img.etimg.com/thumb/width-640,height-480,imgsize-789754,resizemode-1,msid-73320353/the-kirana-is-a-technology-shop-too.jpg",
  },
  {
    name: "Bombay Dyeing",
    desc: "description here",
    img:
      "https://img.etimg.com/thumb/width-640,height-480,imgsize-789754,resizemode-1,msid-73320353/the-kirana-is-a-technology-shop-too.jpg",
  },
  {
    name: "Bombay Dyeing",
    desc: "description here",
    img:
      "https://img.etimg.com/thumb/width-640,height-480,imgsize-789754,resizemode-1,msid-73320353/the-kirana-is-a-technology-shop-too.jpg",
  },
  {
    name: "Bombay Dyeing",
    desc: "description here",
    img:
      "https://img.etimg.com/thumb/width-640,height-480,imgsize-789754,resizemode-1,msid-73320353/the-kirana-is-a-technology-shop-too.jpg",
  },
  {
    name: "Bombay Dyeing",
    desc: "description here",
    img:
      "https://img.etimg.com/thumb/width-640,height-480,imgsize-789754,resizemode-1,msid-73320353/the-kirana-is-a-technology-shop-too.jpg",
  },
  {
    name: "Bombay Dyeing",
    desc: "description here",
    img:
      "https://img.etimg.com/thumb/width-640,height-480,imgsize-789754,resizemode-1,msid-73320353/the-kirana-is-a-technology-shop-too.jpg",
  },
];

export default class Shop extends React.Component {
  render() {
    return (
      <div id="services">
        <h2 className="services-title">Shops</h2>
        <div className="row">
          <Container>
            <Grid container justify="center" spacing={10}>
              {shops.map((shop) => (
                <Grid item>
                  <Paper>
                    <Card style={{ root: { maxWidth: 345 } }}>
                      <CardActionArea>
                        <CardMedia
                          style={{ height: 140 }}
                          image={shop.img}
                          title="shop_image"
                        />
                        <CardContent>
                          <Typography gutterBottom variant="h5" component="h2">
                            {shop.name}
                          </Typography>
                          <Typography
                            variant="body2"
                            color="textSecondary"
                            component="p"
                          >
                            {shop.desc}
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </Paper>
                </Grid>
              ))}
            </Grid>
          </Container>
        </div>
      </div>
    );
  }
}
