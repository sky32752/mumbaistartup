import React, { Component, Fragment } from "react";
import { Typography } from "@material-ui/core";

const TweenMax = window.TweenMax;
const ScrollMagic = window.ScrollMagic;

class Services extends Component {
  constructor(props) {
    super(props);
    this.controller = new ScrollMagic.Controller();
  }
  componentDidMount() {
    this.scene = new ScrollMagic.Scene({
      triggerElement: "#services",
    })
      .setTween(
        TweenMax.staggerFromTo(
          [".services-title", ".service-tag", ".service"],
          1,
          {
            y: 40,
            opacity: 0,
          },
          {
            y: 0,
            opacity: 1,
          },
          0.5
        )
      )
      .addTo(this.controller);
  }

  render() {
    return (
      <Fragment>
        <section
          style={{
            backgroundImage: `url(${require("../img/bg-services.png")})`,
            backgroundRepeat: "no-repeat",
            backgroundColor: "#ecf0f1",
          }}
          className="container-fluid section"
          id="services"
        >
          <h2 className="services-title">Essentials</h2>

          <div className="row">
            <div className="service col p-5">
              <img alt="vegetable" src={require("../img/vegetable.png")} />
              <h4>Vegetables</h4>
            </div>

            <div className="service col p-5">
              <img alt="milk" src={require("../img/milk.png")} />
              <h4>Milk and dairy products</h4>
            </div>
            <div className="service col p-5">
              <img alt="app_development" src={require("../img/grocery.png")} />
              <h4> Groceries</h4>
            </div>
            <div className="service col p-5">
              <img alt="app_development" src={require("../img/banana.png")} />
              <h4>Fruits</h4>
            </div>
            <div className="service col p-5">
              <img alt="app_development" src={require("../img/hibiscus.png")} />
              <h4>Flowers</h4>
            </div>
            <div className="service col p-5">
              <img alt="app_development" src={require("../img/gas.png")} />
              <h4>LPG repair services</h4>
            </div>
          </div>
        </section>

        {/* seconday services  */}

        <section
          style={{
            backgroundImage: `url(${require("../img/bg-services.png")})`,
            backgroundRepeat: "no-repeat",
            backgroundColor: "#ecf0f1",
          }}
          className="container-fluid section"
          id="services"
        >
          <h2 className="services-title">
            Secondary Services (At home delivered by us)
          </h2>

          <div className="row">
            <div className="service col p-5">
              <img alt="app_development" src={require("../img/barber.png")} />
              <h4>Barber</h4>
            </div>

            <div className="service col p-5">
              <img alt="app_development" src={require("../img/plumber.png")} />
              <h4>Plumber</h4>
            </div>
            <div className="service col p-5">
              <img
                alt="app_development"
                src={require("../img/electrician.png")}
              />
              <h4>Electrician</h4>
            </div>
            <div className="service col p-5">
              <img
                alt="app_development"
                src={require("../img/carpenter.png")}
              />
              <h4>Carpenter</h4>
            </div>
            <div className="service col p-5">
              <img
                alt="app_development"
                src={require("../img/vacuum-cleaner.png")}
              />
              <h4>Household cleaning</h4>
            </div>
          </div>
        </section>

        {/* soon serverices */}

        <section
          style={{
            backgroundImage: `url(${require("../img/bg-services.png")})`,
            backgroundRepeat: "no-repeat",
            backgroundColor: "#ecf0f1",
          }}
          className="container-fluid section"
          id="services"
        >
          <h2 className="services-title">Services soon to be available</h2>

          <div className="row">
            <div className="service col p-5">
              <img alt="app_development" src={require("../img/fashion.png")} />
              <h4>Clothes shopping online from local store</h4>
            </div>

            <div className="service col p-5">
              <img alt="app_development" src={require("../img/grater.png")} />
              <h4>Utensils ordering</h4>
            </div>
            <div className="service col p-5">
              <img alt="app_development" src={require("../img/shop.png")} />
              <h4>Getting market items from Kumar galli .</h4>
            </div>
            <div className="service col p-5">
              <img
                alt="app_development"
                src={require("../img/shopping-bag.png")}
              />
              <h4>
                Food delivery with the live coverage of the making of your food
                order.
              </h4>
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

export default Services;
